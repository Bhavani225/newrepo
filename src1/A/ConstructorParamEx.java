package A;

class ParamEx {
	public ParamEx() {
		System.out.println("------this is default constructor------------------");
	}

	public ParamEx(int i) {
		System.out.println("------this is single paramaterised constructor------------------" + i);
	}

	public ParamEx(int i, int j) {
		System.out.println("------this is double paramaterised constructor------------------" + i + "------------" + j);
	}

	public Boolean ParamEx(int i, int j) {
		System.out.println("------this is double paramaterised constructor------------------" + i + "------------" + j);
		return true;
	}

	public ParamEx(int i, String st) {
		System.out
				.println("------this is double paramaterised constructor------------------" + i + "------------" + st);
	}

	public void getSMS() {
		System.out.println("<-------------getSMS----0--------->");
	}

	public void getSMS(int i) {
		System.out.println("<-------------getSMS----1--------i->" + i);
	}

	public void getSMS(int i, int j) {
		System.out.println("<-------------getSMS--2----------i->" + i + "--------j------" + j);
	}

	public void getSMS(int i, String j) {
		System.out.println("<-------------getSMS--3----------i->" + i + "--------j------" + j);
	}
}

public class ConstructorParamEx {
	public static void main(String[] args) {
		ParamEx obj = new ParamEx();
		obj.getSMS();
		obj.getSMS(10);
		obj.getSMS(10, 20);
		obj.getSMS(10, "2344");

		ParamEx obj1 = new ParamEx(10, 20);
		ParamEx obj2 = new ParamEx(10);
		ParamEx obj3 = new ParamEx(10, "abc");

	}
}
