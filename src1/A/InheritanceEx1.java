package A;

class Car2019 {
	public void getBrakes() {
		System.out.println("this is having 2-brakes");
	}

	public void getWeels() {
		System.out.println("this is having 4-Weels");
	}

}

class Car2020 extends Car2019 {
	public void getGare() {
		System.out.println("this is having AUTO-GARE");
	}

}

public class InheritanceEx1 {
	public static void main(String[] args) {
		Car2019 c2019 = new Car2019();// 2
		c2019.getBrakes();
		c2019.getWeels();

		Car2020 c2020 = new Car2020();// 3
		c2020.getBrakes();
		c2020.getWeels();
		c2020.getGare();

		Car2019 c20 = new Car2020();

		// Car2020 c19 = new Car2019();

	}
}
