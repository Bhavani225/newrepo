package A;
/*class Overloading{
	public void m1(int i) {
	System.out.println("----1---"+i);	
	}

	public void m1(int i,int j) {
	System.out.println(i+"----2---"+j);	
	}


	public void m1(int i,String s) {
	System.out.println(i+"----3---"+s);	
	}
	

	public void m1(int i,String s,int j) {
	System.out.println(i+"----4---"+j+"-----"+s);	
	}


}*/
public class OverloadingEx {

	public void m1(int i) {
		System.out.println("----1---" + i);
	}

	public void m1(int i, int j) {
		System.out.println(i + "----2---" + j);
	}

	public void m1(int i, String s) {
		System.out.println(i + "----3---" + s);
	}

	public void m1(int i, String s, int j) {
		System.out.println(i + "----4---" + j + "-----" + s);
	}

	public void m1(int i, String s, int j,int k) {
		System.out.println(i + "----4---" + j + "-----" + s);
	}

	public static void main(String[] args) {
		OverloadingEx obj = new OverloadingEx();
		obj.m1(10, "venkat");
		obj.m1(10, 20);
		obj.m1(10, "venkat", 40);

	}
}
