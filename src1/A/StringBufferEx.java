package A;

public class StringBufferEx {

	public static void main(String[] args) {
		StringBuffer fName = new StringBuffer("Bavani");
		StringBuffer lName = new StringBuffer("Prasad");
		System.out.println(fName);
		System.out.println(lName);
		fName.append(lName);// BavaniPrasad
		System.out.println(fName);

		StringBuffer sb = new StringBuffer(".K");
		fName.append(sb);
		System.out.println(fName);

		StringBuffer s1 = new StringBuffer("Bavani");
		StringBuffer s2 = new StringBuffer("Bavani");

		if (s1 == s2) {// F

			System.out.println(s1.hashCode() + "---------5-True--->" + s2.hashCode());

		} else {
			System.out.println(s1.hashCode() + "---------5-False--->" + s2.hashCode());
		}

		System.out.println(s1.toString());
		System.out.println(s2.toString());
		if (s1.equals(s2)) {// T
			System.out.println(s1 + "------3--True---" + s2);
		} else {
			System.out.println(s1 + "------4--False----" + s2);
		}
	}

}
