package A;

public class StringClassMethodsEx {

	public static void main(String[] args) {
		String s = "PRASAD   ".trim();
		String s1 = "prasad";
		String s2 = "PrAsAd";
		String s3 = "pRaSad";

		String s4 = "bavani";
		System.out.println(s4);// bavani
		String s5 = s4;
		System.out.println(s5);// bavani
		System.out.println(s2.toUpperCase());
		System.out.println(s2.toLowerCase());

		if (s2 == s3) {// F
			System.out.println(s.hashCode() + "---------1-True--->" + s1.hashCode());
		} else {
			System.out.println(s.hashCode() + "---------1-False--->" + s1.hashCode());
		}
		if (s2.equals(s3)) {// F
			System.out.println("------3--True");
		} else {
			System.out.println("------4--False");
		}

		if (s2.equalsIgnoreCase(s3)) {// T
			System.out.println("------3--True");
		} else {
			System.out.println("------4--False");
		}
		if (s == s1) {// F
			System.out.println(s.hashCode() + "---------1-True--->" + s1.hashCode());
		} else {
			System.out.println(s.hashCode() + "---------1-False--->" + s1.hashCode());
		}
		if (s.equals(s1)) {// F
			System.out.println("------3--True");
		} else {
			System.out.println("------4--False");
		}

		if (s.equalsIgnoreCase(s1)) {// T
			System.out.println("------3--True");
		} else {
			System.out.println("------4--False");
		}

	}

}
