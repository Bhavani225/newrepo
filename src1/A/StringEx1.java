package A;

public class StringEx1 {

	public static void main(String[] args) {
		String s = "abc";
		String s1 = "abc";
		String s2 = new String("abc");
		String s6 = new String("abc");
		String s3 = new String("xyz");
		String s4 = "pqr";
		String s5 = s.concat("mno");

		if (s4.equals(s5)) {// F
			System.out.println("------3--True");
		} else {
			System.out.println("------4--False");
		}

		if (s4 == s5) {// F

			System.out.println(s.hashCode() + "---------5-True--->" + s2.hashCode());

		} else {
			System.out.println(s.hashCode() + "---------5-False--->" + s2.hashCode());
		}

		if (s == s1) {// T
			System.out.println(s.hashCode() + "---------1-True--->" + s1.hashCode());
		} else {
			System.out.println(s.hashCode() + "---------1-False--->" + s1.hashCode());
		}
		if (s.equals(s1)) {// T
			System.out.println("------3--True");
		} else {
			System.out.println("------4--False");
		}
		if (s == s2) {// T

			System.out.println(s.hashCode() + "---------5-True--->" + s2.hashCode());

		} else {
			System.out.println(s.hashCode() + "---------5-False--->" + s2.hashCode());
		}
		if (s.equals(s2)) {// T
			System.out.println("---6-------True");
		} else {
			System.out.println("----------7---------False");
		}

		if (s6 == s2) {// T

			System.out.println(s6.hashCode() + "---------5-True--->" + s2.hashCode());

		} else {
			System.out.println(s6.hashCode() + "---------5-False--->" + s2.hashCode());
		}

	}

}
