package A;

class Test1 {
	public void getValues(int i) {
		System.out.println(i);
	}

	public void getValues(int i, int j) {
		System.out.println(i + "------" + j);
	}

	public void getValues(int i, int j, int k) {
		System.out.println(i + "------------" + j + "--------" + k);
	}

}

public class PolimorphisamEx {
	public static void main(String[] args) {
		Test1 t = new Test1();
		t.getValues(10, 20);

	}
}
