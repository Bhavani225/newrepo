package A;

public class StringBufferEx2 {

	public static void main(String[] args) {
		String s = new String();// 0
		System.out.println(s.length());

		StringBuffer sb = new StringBuffer();// 16
		System.out.println(sb.capacity());

		StringBuffer sb1 = new StringBuffer();// 16
		sb.append(sb1);
		System.out.println(sb.capacity());// 16

		StringBuffer fName = new StringBuffer("Bavani");
		System.out.println(fName.capacity());// 22

	}

}
