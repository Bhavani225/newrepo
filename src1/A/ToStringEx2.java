package A;

class Emp {
	int eid;
	String ename;

	public Emp(int eid, String ename) {
		this.eid = eid;
		this.ename = ename;
	}

	@Override
	public String toString() {
		return eid + "-----" + ename;
	}

	public void getValues() {
		System.out.println("hi...");
	}
}

public class ToStringEx2 {
	/*
	 * @Override public String toString() { // TODO Auto-generated method stub
	 * return super.toString(); }
	 */
	public static void main(String[] args) {
		Emp e1 = new Emp(10, "Prasad");
		Emp e2 = new Emp(20, "Venkat");
		System.out.println(e1);// A.Emp@1234//e1.toString()
		System.out.println(e2);// A.Emp@1234//e2.toString()
		e1.getValues();
		e2.getValues();

		System.out.println(e1.hashCode());
		System.out.println(e2.hashCode());

	}
}
