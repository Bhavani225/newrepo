package A;

class Test2 {
	public void m1(int i, int j) {
		System.out.println(i + "----t2--------" + j);
	}
}

class Test3 extends Test2 {

	public void m1(int i, int j) {
		System.out.println(i + "-----t3-------" + j);
	}
}

public class OverridingEx {
	public static void main(String[] args) {
		Test2 t2 = new Test2();
		t2.m1(10, 20);

		Test3 t3 = new Test3();
		t3.m1(30, 40);

		Test2 t23 = new Test3();
		t23.m1(50, 60);

	}
}
