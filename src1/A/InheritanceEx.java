package A;

class Parent {
	int i = 10;

	public void getSum() {
		// TODO Auto-generated method stub

	}

}

class Child extends Parent {
	int j = 20;

	public void getSum() {
		int add = i + j;
		System.out.println(add);
	}

}

class SubClild extends Child {
	int k = 30;
}

public class InheritanceEx {
	public static void main(String[] args) {
		Parent p = new Parent();// 2
		System.out.println(p.i);// 10

		Child c = new Child();// 2
		System.out.println(c.j);// 20
		System.out.println("-----1------------");
		Child c1 = new Child();// 3
		System.out.println(c1.j);// 20
		System.out.println(c1.i);// 10
		System.out.println("-------2----------");
		Parent cp = new Child();
		cp.getSum();
		/*
		 * Child pc = new Parent(); int j=0,n;
		 * 
		 * System.out.println(j);//0
		 * 
		 * for (j = 1; j <=10; j++) { System.out.println(j);//0 }
		 */}
}
