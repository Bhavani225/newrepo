package B;

interface Engine {
	public abstract void getEngine();
}

interface Vihicle extends Engine {
	int i = 10;// public static final int i =10;

	public void getSeats();// abstract public void getSets()

	public abstract void getSize();

	abstract public void getPickUP();
}

interface Vihicle1 {
	abstract public void getAutoLock();
}

class BusImpl implements Vihicle, Vihicle1 {

	@Override
	public void getSeats() {
		System.out.println("it is having 50-seats.....");
	}

	@Override
	public void getSize() {
		System.out.println("It is very big....");

	}

	@Override
	public void getPickUP() {
		System.out.println("SpeedPickUP is slow...>");

	}

	@Override
	public void getAutoLock() {
		System.out.println("Bus -Auto lock....>");

	}

	@Override
	public void getEngine() {
		System.out.println("Bus it is having big engeen capacticy..");

	}

}

class Carimp implements Vihicle, Vihicle1 {

	@Override
	public void getAutoLock() {
		System.out.println("car having this Autolock ");

	}

	@Override
	public void getSeats() {
		System.out.println("it has 6 seats");

	}

	@Override
	public void getSize() {
		System.out.println("it is small then Bus");
	}

	@Override
	public void getPickUP() {
		System.out.println("this will pickup fast then bus ");
	}

	@Override
	public void getEngine() {
		System.out.println("this will small engeen then bus ");

	}

}

public class InterfaceEx {
	public static void main(String[] args) {
		// Vihicle v = new Vihicle();// we cont create direct object for
		// interface
		BusImpl busObj = new BusImpl();
		busObj.getSeats();
		busObj.getPickUP();
		busObj.getSize();

		Carimp carObj = new Carimp();
		carObj.getAutoLock();
		carObj.getPickUP();
		carObj.getSeats();
		System.out.println(carObj.i);

	}
}
