package B;

public class AnimalMain {
	public static void main(String[] args) {
		System.out.println("----------Dog-Details----------");
		Dog d = new Dog();
		d.getLegges();
		d.getTail();
		d.getHears();
		d.getFriendly();
		d.getHumans();
		d.getPet();
		System.out.println("----------Tiger-Details----------");
		Tiger t = new Tiger();
		t.getLegges();
		t.getTail();
		t.getHears();
		t.getFriendly();
		t.getHumans();
		t.getPet();
	}
}
