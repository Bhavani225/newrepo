package B;

abstract class Animal {

	public void getLegges() {
		System.out.println("both are having 4-legges");
	}

	public void getTail() {
		System.out.println("both are habing 1-tail");
	}

	public void getHears() {
		System.out.println("both are having 2-hears");
	}

	abstract public void getPet();

	abstract public void getHumans();

	abstract public void getFriendly();
}



class Dog extends Animal {

	@Override
	public void getPet() {
		System.out.println("this is pet-Dog");

	}

	@Override
	public void getHumans() {
		System.out.println("it wont eats humans");

	}

	@Override
	public void getFriendly() {
		System.out.println("it is Friendly natur");

	}

}

class Tiger extends Animal {

	@Override
	public void getPet() {
		System.out.println("this is un-pet-Tiger");

	}

	@Override
	public void getHumans() {
		System.out.println("it will eats humans");

	}

	@Override
	public void getFriendly() {
		System.out.println("it is not a Friendly natur");

	}

}